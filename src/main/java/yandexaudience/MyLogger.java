package yandexaudience;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.FileHandler;
import java.util.logging.SimpleFormatter;
import java.util.logging.ConsoleHandler;
import java.io.File;
import java.io.IOException;

public class MyLogger {

    public static final Level ERROR = Level.SEVERE;
    public static final Level WARN = Level.WARNING;
    public static final Level INFO = Level.INFO;
    public static final Level DEBUG = Level.FINE;
    private Logger _logger;


    public MyLogger (String classname) {
        _logger = Logger.getLogger(classname);
    }

    public void setLevel(Level level) {
        this._logger.setLevel(level);
    }

    public void error(Object toLog) {
        this._logger.log(ERROR, toLog.toString());
    }

    public void warn(Object toLog) {
        this._logger.log(WARN, toLog.toString());
    }

    public void info(Object toLog) {
        this._logger.log(INFO, toLog.toString());
    }

    public void debug(Object toLog) {
        this._logger.log(DEBUG, toLog.toString());
    }

    public void log(Level level, Object toLog) {
        this._logger.log(level, toLog.toString());
    }
}