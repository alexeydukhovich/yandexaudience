package yandexaudience;

import com.google.gson.*;

import java.io.*;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.security.MessageDigest;
import java.nio.charset.StandardCharsets;

import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.enums.CSVReaderNullFieldIndicator;


public class YandexAudience
{

    private String token = "";

    private static final String URL_CONFIRM = "https://api-audience.yandex.ru/v1/management/segment/%d/confirm";
    private static final String URL_UPLOAD_FILE = "https://api-audience.yandex.ru/v1/management/segments/upload_file";
    private static final String URL_UPLOAD_CSV_FILE = "https://api-audience.yandex.ru/v1/management/segments/upload_csv_file";

    private MyLogger _logger;
    private static final String boundary = "----7MA4YWxkTrZu0gW";

    public static void main( String[] args ) {
    }

    /**
     * Simple constructor.
     */
    public YandexAudience() {
        this._logger = new MyLogger(YandexAudience.class.getName());
        this._logger.setLevel(MyLogger.DEBUG);
    }

    /**
     * Sets access token.
     *
     * <p>This token will be used in requests.</p>
     *
     * @param new_token
     */
    public void setToken(String new_token) {
        token = new_token;
    }

    /**
     * Uploads a segment to Yandex.Audience from a file with one column of data.
     *
     * <p>The file must contain one entry per line. The file must contain at least 1000 entries.</p>
     * <p>Note: you should set token before call this method</p>
     * <p>Content types:</p>
     * <ul>
     *     <li><tt>"email"</tt> <i>(Email addresses)</i> - string of Latin characters, including the @ symbol and domain name. No uppercase letters allowed. Example: example@yandex.com.</li>
     *     <li><tt>"phone"</tt> <i>(Mobile phone numbers)</i> - string of numbers that contains the mobile phone number and country code. Spaces and additional symbols are not allowed. Example: 79995551111.</li>
     *     <li><tt>"idfa_gaid"</tt></li>
     *     <ul>
     *         <li><i>(Android (GAID))</i> - lowercase alpha-numeric string separated by hyphens. Example: aaaaaaaa-bbbb-cccc-1111-222222222200.</li>
     *         <li><i>(iOS (IDFA))</i> - uppercase alpha-numeric string separated by hyphens. Example: AAAAAAAAA-BBBB-CCCC-1111-222222220000.</li>
     *     </ul>
     *     <li><tt>"mac"</tt> <i>(MAC Addresses)</i> - string that contains only hex symbols. Example: AE123456D0A1</li>
     * </ul>
     * @param   filepath    The path to the data file.
     * @param   _hash        Hash the data or not.
     * @param   _hashed      Whether the file was hashed or not.
     * @param   _with_header Whether the file has a header or not.
     * @param   segment_name Name of the segment.
     * @param   content_type Type of the content.
     * @return              Created segment ID
     */
    public int upload_file(String filepath, String _hash, String _hashed, String _with_header, String segment_name, String content_type) {

        boolean hash = _hash.equals("true");
        boolean hashed = _hashed.equals("true");
        boolean with_header = _with_header.equals("true");

        File file = new File(filepath);

        List<String[]> file_contents;

        //
        // Reading file
        //

        try {

            _logger.info("Reading file: " + file.getAbsolutePath());
            file_contents = read_csv(file, with_header);
            _logger.info("File successfully read");

        } catch (Exception e){
            _logger.error(e.toString());
            e.printStackTrace();
            return -1;
        }

        //
        // Hashing entries
        //

        if ((hash) && (!hashed)) {
            try {
                _logger.info("Hashing file");
                file_contents = hash(file_contents, 0, (content_type.equals("mac")) ? ("mac") : ("other"));
                _logger.info("File successfully hashed");

            } catch (Exception e) {
                _logger.error(e.toString());
                e.printStackTrace();
                return -1;
            }
        }

        //
        // Sending file
        //

        JsonObject json_response;
        int segmentId;

        try {
            _logger.info("Sending of the segment.");
            _logger.info("Connect to " + URL_UPLOAD_FILE);

            json_response = send(token, false, convert_file_contents_to_string(file_contents));
            segmentId = json_response.getAsJsonObject("segment").get("id").getAsInt();

            _logger.info("Segment successfully sent");
            _logger.info("Segment ID: " + segmentId);

        } catch (Exception e) {
            _logger.error(e.toString());
            e.printStackTrace();
            return -1;
        }

        //
        // Confirming file
        //

        try {
            _logger.info("Confirming of the segment.");
            _logger.info("Connect to " + String.format(URL_CONFIRM, segmentId));

            confirm(token, segmentId, segment_name, hash || hashed, content_type);

            _logger.info("Segment successfully confirmed.");

        } catch (Exception e) {
            _logger.error(e.toString());
            e.printStackTrace();
            return -1;
        }

        return segmentId;
    }

    /**
     * Uploads a segment to Yandex.Audience from a file with multiple columns of data.
     *
     * <p>The file must contain one entry per line. The file must contain at least 1000 entries.</p>
     * <p>The file must contain at least one field of the two fields: email and phone.</p>
     * <p>Content fields:</p>
     * <ul>
     *     <li><tt>"email"</tt> <i>(Email addresses)</i> - string of Latin characters, including the @ symbol and domain name. No uppercase letters allowed. Example: example@yandex.com.</li>
     *     <li><tt>"phone"</tt> <i>(Mobile phone numbers)</i> - string of numbers that contains the mobile phone number and country code. Spaces and additional symbols are not allowed. Example: 79995551111.</li>
     *     <li><tt>other fields</tt> <i>(Example: external_id)</i> - Any other fields.</li>
     * </ul>
     * @param   filepath    The path to the data file.
     * @param   _hash        Hash the data or not.
     * @param   _hashed      Whether the file was hashed or not.
     * @param   _with_header Whether the file has a header or not.
     * @param   segment_name Name of the segment.
     * @param   schema      Schema of the content in JSON Array format.
     * @return              Created segment ID
     */
    public int upload_csv_file(String filepath, String _hash, String _hashed, String _with_header, String segment_name, String schema) {

        boolean hash = _hash.equals("true");
        boolean hashed = _hashed.equals("true");
        boolean with_header = _with_header.equals("true");

        File file = new File(filepath);

        List<String[]> file_contents;
        String[] schema_array;

        //
        // Reading file
        //

        try {

            _logger.info("Reading file: " + file.getAbsolutePath());
            file_contents = read_csv(file, with_header);
            _logger.info("File successfully read");

        } catch (Exception e){
            _logger.error(e.toString());
            e.printStackTrace();
            return -1;
        }

        //
        // Parsing schema
        //

        try {

            _logger.info("Parsing schema: " + schema);
            schema_array = parse_schema(schema);
            _logger.info("Schema successfully parsed");

        } catch (Exception e){
            _logger.error(e.toString());
            e.printStackTrace();
            return -1;
        }

        //
        // Hashing entries
        //

        if ((hash) && (!hashed)) {
            try {
                _logger.info("Hashing file");
                for (int i = 0; i < schema_array.length; i++) {
                    if ((schema_array[i].equals("phone")) || (schema_array[i].equals("email")))
                        file_contents = hash(file_contents, i, "other");
                }
                _logger.info("File successfully hashed");

            } catch (Exception e) {
                _logger.error(e.toString());
                e.printStackTrace();
                return -1;
            }
        }
        else {
            if (hash)
                _logger.warn("Because hashed=true, file will not be hashed");
            else
                _logger.info("Hashing of the file is not required");
        }

        //
        // Adding header
        //

        file_contents.add(0, schema_array);

        //
        // Sending file
        //

        JsonObject json_response;
        int segmentId = -1;

        try {
            _logger.info("Sending of the segment.");
            _logger.info("Connect to " + URL_UPLOAD_CSV_FILE);

            json_response = send(token, true, convert_file_contents_to_string(file_contents));
            segmentId = json_response.getAsJsonObject("segment").get("id").getAsInt();

            _logger.info("Segment successfully sent");
            _logger.info("Segment ID: " + segmentId);

        } catch (Exception e) {
            _logger.error(e.toString());
            e.printStackTrace();
            return -1;
        }

        //
        // Confirming file
        //

        try {
            _logger.info("Confirming of the segment.");
            _logger.info("Connect to " + String.format(URL_CONFIRM, segmentId));

            json_response = confirm(token, segmentId, segment_name, hash || hashed, "crm");

            _logger.info("Segment successfully confirmed.");

        } catch (Exception e) {
            _logger.error(e.toString());
            e.printStackTrace();
            return -1;
        }

        return segmentId;
    }

    private List<String[]> read_csv (File file, boolean with_header) throws Exception {
        PushbackInputStream pbis = new PushbackInputStream(new FileInputStream(file.getAbsolutePath()), 3);
        skip_bom(pbis);
        CSVParser parser = new CSVParserBuilder()
                .withSeparator(',')
                .withIgnoreQuotations(true)
                .withFieldAsNull(CSVReaderNullFieldIndicator.BOTH)
                .build();
        CSVReader reader = new CSVReaderBuilder(new BufferedReader(new InputStreamReader(pbis, "UTF8")))
                    .withCSVParser(parser)
                    .withSkipLines((with_header) ? 1 : 0)
                    .build();

        List<String[]> file_contents = reader.readAll();
        reader.close();

        return file_contents;
    }

    private String convert_file_contents_to_string(List<String[]> file_contents) {
        StringBuilder sb = new StringBuilder();

        for (String[] entry : file_contents) {
            String sep = "";
            for (String field : entry) {
                sb.append(sep);
                sep = ",";
                sb.append(field);
            }
            sb.append("\r\n");
        }

        return sb.toString();
    }

    private String[] parse_schema(String schema_str) throws Exception {

        //
        // Parsing
        //

        JsonArray array = new JsonParser().parse(schema_str).getAsJsonArray();
        List<String> schema = new ArrayList<String>();

        for (JsonElement field : array)
            schema.add(field.getAsString());

        //
        // Checking
        //

        if ((!(schema.contains("email")) && !(schema.contains("phone"))) || (schema.contains(null))) {
            throw new Exception();
        }

        return schema.toArray(new String[schema.size()]);
    }

    private JsonObject confirm(String token, int segmentId, String segment_name, boolean hashed, String content_type) throws Exception {

        JsonObject json_body = new JsonObject();
        JsonObject json_segment = new JsonObject();

        json_segment.addProperty("id", segmentId);
        json_segment.addProperty("name", segment_name);
        json_segment.addProperty("hashed", hashed);
        json_segment.addProperty("content_type", content_type);

        json_body.add("segment", json_segment);

        URL url = new URL(String.format(URL_CONFIRM, segmentId));

        // Preparing headers
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "OAuth " + token);
        headers.put("Content-Type", "application/json");

        // Preparing body
        String body = json_body.toString();

        return post_request(url, headers, body);
    }

    private JsonObject send (String token, boolean isCRM, String file_contents) throws Exception {

        URL url;
        if (!isCRM)
            url = new URL(URL_UPLOAD_FILE);
        else
            url = new URL(URL_UPLOAD_CSV_FILE);

        // Preparing headers
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "OAuth " + token);
        headers.put("Content-Type", "multipart/form-data; boundary=" + boundary);

        // Preparing body
        String body =
                "--" + boundary + "\r\n" +
                "Content-Disposition: form-data; name=\"file\"; filename=\"data.csv\"\r\n" +
                "Content-Type: application/octet-stream\r\n" +
                "\r\n" +
                file_contents +
                "--" + boundary + "--" + "\r\n";

        return post_request(url, headers, body);
    }

    private JsonObject post_request(URL url, Map<String, String> headers, String body) throws Exception {

        // Request
        HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
        connection.setDoOutput(true);
        connection.setRequestMethod("POST");

        for (Map.Entry<String, String> entry : headers.entrySet()) {
            connection.setRequestProperty(entry.getKey(), entry.getValue());
        }

        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream(), "UTF-8"));
        bw.write(body);
        bw.flush();
        bw.close();
        connection.connect();

        // Response
        StringBuilder sb_response = new StringBuilder();
        int responseCode = connection.getResponseCode();

        try(BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF8"))) {
            for (String inputLine = br.readLine(); inputLine != null; inputLine = br.readLine())
                sb_response.append(inputLine);
        } catch (IOException e) {
            try (BufferedReader br = new BufferedReader(new InputStreamReader(connection.getErrorStream(), "UTF8"))) {
                for (String inputLine = br.readLine(); inputLine != null; inputLine = br.readLine())
                    sb_response.append(inputLine);
                JsonObject json_response = new JsonParser().parse(sb_response.toString()).getAsJsonObject();
                log_json_response(responseCode, json_response);
                throw e;
            }
        }

        JsonObject json_response = new JsonParser().parse(sb_response.toString()).getAsJsonObject();
        log_json_response(responseCode, json_response);

        return json_response;
    }

    private List<String[]> hash(List<String[]> file_contents, int index, String content_type) throws Exception {

        List<String[]> result = new ArrayList<String[]>();

        switch (content_type) {
            case "mac":
                for (String[] entry : file_contents) {
                    String[] new_entry = new String[entry.length];
                    for (int i = 0; i < entry.length; i++) {
                        if (i == index)
                            new_entry[i] = md5_mac(entry[i]);
                        else
                            new_entry[i] = entry[i];
                    }
                    result.add(new_entry);
                }
                break;
            case "other":
                for (String[] entry : file_contents) {
                    String[] new_entry = new String[entry.length];
                    for (int i = 0; i < entry.length; i++) {
                        if (i == index)
                            new_entry[i] = md5(entry[i]);
                        else
                            new_entry[i] = entry[i];
                    }
                    result.add(new_entry);
                }
                break;
            default:
                throw new Exception();
        }

        return result;
    }

    private void log_json_response(int status, JsonObject body) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String prettyJson = gson.toJson(body);
        if ((status == 200) || (status == 201)) {
            _logger.info("Response status: " + status);
            _logger.info(prettyJson);
        }
        else {
            _logger.error("Response status: " + status);
            _logger.error(prettyJson);
        }
    }

    private void skip_bom(PushbackInputStream pbis) throws Exception {
        byte[] data = new byte[3];

        pbis.read(data,0,3);

        if (!((data[0] == (byte)0xEF) && (data[1] == (byte)0xBB) && (data[2] == (byte) 0xBF))) {
            pbis.unread(data);
        }
    }

    private String md5(String message) {
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            byte[] hash = digest.digest(message.getBytes(StandardCharsets.UTF_8));
            return toHex(hash);
        } catch (Exception e) {
            return null;
        }
    }

    private String toHex(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (byte b : bytes) {
            sb.append(String.format("%1$02x", b));
        }
        return sb.toString();
    }

    private String md5_mac(String mac) {
        try {
            int len = mac.length();
            byte[] data = new byte[len / 2];
            for (int i = 0; i < len; i += 2) {
                data[i / 2] = (byte) ((Character.digit(mac.charAt(i), 16) << 4)
                        + Character.digit(mac.charAt(i+1), 16));
            }

            MessageDigest digest = MessageDigest.getInstance("MD5");
            byte[] hash = digest.digest(data);
            return toHex(hash);
        } catch (Exception e) {
            return null;
        }
    }

}
